/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import dto.Cliente;

/**
 *
 * @author QBEX
 */
public class Sesion {
private boolean estado;
private int idcliente;

    public Sesion() {
        this.estado = false;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

  public void cerrarSesion(){
      this.setEstado(false);
      this.idcliente=0;
  }

    @Override
    public String toString() {
        return "Sesion{" + "estado=" + estado + ", idcliente=" + idcliente + '}';
    }
    
    


}
