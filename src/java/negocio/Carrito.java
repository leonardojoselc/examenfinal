/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import dto.*;
import java.time.LocalDate;
import java.util.List;
import java.util.TreeSet;

/**
 *
 * @author estudiante
 */
public class Carrito {
    private int id;
    private int idCliente;
    private TreeSet<Item> items=new TreeSet();
    //Debe ser tomada del sistema
    private LocalDate fecha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    

    
    public TreeSet<Item> getItems() {
        return items;
    }

    public void setItems(TreeSet<Item> items) {
        this.items = items;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Carrito() {
    }
    
     public boolean insertarItem(Item nuevo)
    {
      return(this.items.add(nuevo));
  
    }
    
   
    
}

    

