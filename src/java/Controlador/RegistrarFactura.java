/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import dao.ClienteJpaController;
import dao.Conexion;
import dao.FacturaJpaController;
import dto.Cliente;
import dto.Factura;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author QBEX
 */
@WebServlet(name = "RegistrarFactura", urlPatterns = {"/RegistrarFactura.do"})
public class RegistrarFactura extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
          try {
        String ide=request.getParameter("id");
        String cliente=request.getParameter("cliente");
        //String fecha=request.getParameter("fecha");
        
        
        int id=Integer.parseInt(ide);
        int idCliente=Integer.parseInt(cliente);
       Factura factura= new Factura(id);
       
        
        Conexion con=Conexion.getConexion();
        ClienteJpaController clienteDao=new ClienteJpaController(con.getBd());
        
        factura.setCedula(clienteDao.findCliente(idCliente));
        FacturaJpaController facturaDao=new FacturaJpaController(con.getBd());
        facturaDao.create(factura);
        factura.setCedula(clienteDao.findCliente(1));
        facturaDao.edit(factura);
        //List<> clientes=clienteDao.findClienteEntities();
        
       // request.getSession().setAttribute("clientes", clientes);
        request.getRequestDispatcher("./cliente/registrar.jsp").forward(request, response);
            
        } catch (Exception ex) {
            
            request.getSession().setAttribute("error", ex);
            Logger.getLogger(Registrar.class.getName()).log(Level.SEVERE, null, ex);
            request.getRequestDispatcher("./cliente/error.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
