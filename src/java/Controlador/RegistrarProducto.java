/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import dao.Conexion;
import dao.ItemJpaController;
import dao.ProductoJpaController;
import dto.Cliente;
import dto.Factura;
import dto.Item;
import dto.Producto;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ufps.util.SubirArchivoBean;

/**
 *
 * @author QBEX
 */
@WebServlet(name = "RegistrarProducto", urlPatterns = {"/RegistrarProducto.do"})
public class RegistrarProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
        String nombre=request.getParameter("nombre");
        String ide=request.getParameter("id");
        String cant=request.getParameter("cantidad");
        String pre=request.getParameter("precio");
       // String file=request.getParameter("archivo");
        
        int id=Integer.parseInt(ide);
        float precio=Float.parseFloat(pre);
        int cantidad= Integer.parseInt(cant);
        
       
        //Factura fact =new Factura(3);
        //item.setIdFactura(fact);
        Conexion con=Conexion.getConexion();
        
        ProductoJpaController productoDao=new ProductoJpaController(con.getBd());
        //ItemJpaController itemDao=new ItemJpaController(con.getBd());
        String dir= (String)request.getSession().getAttribute("dir");
       
        SubirArchivoBean subir=(SubirArchivoBean)request.getSession().getAttribute("subir");
       // subir.subirFile();
        Producto nuevo=new Producto(id,nombre,cantidad,precio,"/web/imagenes/"+subir.getNombreArchivo());
        Item item=new Item((id+1),nuevo,cantidad);
        //itemDao.create(item);
         productoDao.create(nuevo);
        List<Producto> productos=productoDao.findProductoEntities();
       // List<Item> items=itemDao.findItemEntities();
        //request.getSession().setAttribute("items", items);
        request.getSession().setAttribute("productos", productos);
        request.getRequestDispatcher("./producto/registrarProducto.jsp").forward(request, response);
            
        } catch (Exception ex) {
            
            request.getSession().setAttribute("error", ex);
            Logger.getLogger(Registrar.class.getName()).log(Level.SEVERE, null, ex);
            request.getRequestDispatcher("./producto/error.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
