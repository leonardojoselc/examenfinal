/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util_vista;

import dto.Cliente;
import dto.Producto;
import java.util.List;

/**
 *
 * @author madarme
 */
public class Tabla<T> {

    public Tabla() {
    }
    
    
   public String getTablaClientes(List<Cliente> objetos) 
   {
   
       
   if(objetos.isEmpty())    
       return "";
   
   String msg="\n<table>";
   
   msg+="\n<tr>";
   
   msg+="\n<th scope='col'>Cedula</th>";
   msg+="\n<th scope='col'>Nombre</th>";
   msg+="\n<th scope='col'>Email</th>";
   msg+="\n<th scope='col'>Dirección</th>";
   msg+="\n<th scope='col'>Télefono</th>";
   msg+="\n</tr>";
   
   for(Cliente c:objetos)
   {
       msg+="\n<tr>";
       msg+="\n<td>"+c.getCedula()+"</td>";
       msg+="\n<td>"+c.getNombre()+"</td>";
       msg+="\n<td>"+c.getEmail()+"</td>";
       msg+="\n<td>"+c.getDireccion()+"</td>";
       msg+="\n<td>"+c.getTelefono()+"</td>";
       msg+="\n</tr>";
   }
   
   msg+="\n</table>";
   return msg;
   }
    
   
   public String getTablaProductos(List<Producto> objetos) 
   {
   
       
   if(objetos.isEmpty())    
       return "";
   
   String msg="\n<table>";
   
   msg+="\n<tr>";
   
   msg+="\n<th scope='col'>Id</th>";
   msg+="\n<th scope='col'>Nombre</th>";
   msg+="\n<th scope='col'>cantidad</th>";
   msg+="\n<th scope='col'>Precio</th>";
   msg+="\n<th scope='col'>url</th>";
   msg+="\n</tr>";
   
   for(Producto p:objetos)
   {
       msg+="\n<tr>";
       msg+="\n<td>"+p.getIdProducto()+"</td>";
       msg+="\n<td>"+p.getNombre()+"</td>";
       msg+="\n<td>"+p.getCantidad()+"</td>";
       msg+="\n<td>"+p.getPrecio()+"</td>";
       msg+="\n<td>Url</td>";
       msg+="\n</tr>";
   }
   
   msg+="\n</table>";
   return msg;
   }
    
   
   public String getTablaCatalogo(List<Producto> objetos) 
   {
   
       
   if(objetos.isEmpty())    
       return "";
   
   String msg="\n<table>";
   
   msg+="\n<tr>";
   
   msg+="\n<th scope='col'>Nombre</th>";
   msg+="\n<th scope='col'>Disponibles</th>";
   msg+="\n<th scope='col'>Precio</th>";
   msg+="\n<th scope='col'>Compre Aqui</th>";
   
   msg+="\n</tr>";
   
   for(Producto p:objetos)
   {
       msg+="\n<form action='../AddAcarrito.do'><tr>";
       msg+="\n<td>"+p.getNombre()+"<input type='hidden' value='"+p.getIdProducto()+"' name='idProducto'</td>";
       msg+="\n<td>"+p.getCantidad()+"</td>";
       msg+="\n<td>"+p.getPrecio()+"</td>";
       msg+="\n<td><input class='register-input' width='10px' type='number' name='cantidad' value='cantidad'/><input type='submit' value='Agegar a Carrito' name='registrar' class='register-button'/></td>";
       msg+="\n</tr></form>";
   }
   
   msg+="\n</table>";
   return msg;
   }
   
}
