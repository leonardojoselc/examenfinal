<%-- 
    Document   : registrar
    Created on : 11/06/2019, 09:44:47 AM
    Author     : estudiante
--%>

<%@page import="util_vista.Tabla"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dto.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilo.css">
        <title>Pag de Registro de Clientes</title>
    </head>
    <body>
        
      <%
      List<Cliente> c=(List<Cliente>)request.getSession().getAttribute("clientes");
      Tabla t=new Tabla();
      String tabla=t.getTablaClientes(c);
      %>  
        
        <h1 class="register-title">El Registro se realizó con éxito</h1>
       
        <div class="div_centrado">
            <center>
        <%=tabla%>
        </center>
        </div>
        
    <center>
        <p><a href="./cliente/registrar.html"> Registrar otro</a></p>     
     </center>
    </body>
</html>
