<%-- 
    Document   : registrarProducto
    Created on : 17/06/2019, 04:01:02 PM
    Author     : QBEX
--%>


<%@page import="util_vista.Tabla"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dto.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilo.css">
        <title>Pag de Registro de Productos</title>
    </head>
    <body>
        
      <%
      List<Producto> p=(List<Producto>)request.getSession().getAttribute("productos");
      Tabla t=new Tabla();
      String tabla=t.getTablaProductos(p);
      %>  
        
        <h1 class="register-title">El Registro se realizó con éxito</h1>
       
        <div class="div_centrado">
            <center>
        <%=tabla%>
        </center>
        </div>
        
    <center>
        <p><a href="./producto/registroProducto.html"> Registrar otro</a></p>     
     </center>
    </body>
</html>
